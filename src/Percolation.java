import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private static final int VIRTUAL_TOP = 0;
    private final int virtualBottom;
    private final int dimension;
    private boolean[] site;
    private int openCounter;
    private final WeightedQuickUnionUF quickUnion;


    private int itemId(int row, int col) {
        return (row - 1) * dimension + col;
    }

    public Percolation(int n) {

        if (n <= 0) {
            throw new java.lang.IllegalArgumentException();
        }

        // create n-by-n grid, with all sites blocked
        site = new boolean[n * n + 2];
        // all sites blocked

        this.virtualBottom = n * n + 1;

        this.dimension = n;
        this.openCounter = 0;

        // create Weighted Quick Union UF
        // +2 to include two virtual sites,
        // one on the top and one on the bottom
        quickUnion = new WeightedQuickUnionUF(n * n + 2);

    }

    public void open(int row, int col) {

        if (row < 1 || col < 1 || row > dimension || col > dimension) {
            throw new java.lang.IllegalArgumentException();
        }

        int item = itemId(row, col);
        // open site (row, col) if it is not open already
        if (!site[item]) {
            site[item] = true;
            this.openCounter++;
            // rows
            // if site is on the top, connect with virtual top site
            if (row == 1) {
                quickUnion.union(item, VIRTUAL_TOP);
            }

            // if site is on the bottom, connect with virtual bottom site
            if (row == dimension) {
                quickUnion.union(item, virtualBottom);
            }

            // if ( isOpen(row-1, col) ) quickUnion.union(item, itemId(row-1, col));
            if (row > 1 && isOpen(row - 1, col)) {
                quickUnion.union(item, itemId(row - 1, col));
            }
            if (row < dimension && isOpen(row + 1, col)) {
                quickUnion.union(item, itemId(row + 1, col));
            }

            // cols
            if (col > 1 && isOpen(row, col - 1)) {
                quickUnion.union(item, itemId(row, col - 1));
            }
            if (col < dimension && isOpen(row, col + 1)) {
                quickUnion.union(item, itemId(row, col + 1));
            }
        }

    }

    public boolean isOpen(int row, int col) {
        // is site (row, col) open?
        if (row < 1 || col < 1 || row > dimension || col > dimension) {
            throw new java.lang.IllegalArgumentException();
        }

        return site[itemId(row, col)];
    }

    public boolean isFull(int row, int col) {
        // is site (row, col) full?
        if (row < 1 || col < 1 || row > dimension || col > dimension) {
            throw new java.lang.IllegalArgumentException();
        }

        return quickUnion.connected(itemId(row, col), VIRTUAL_TOP);
    }

    public int numberOfOpenSites() {
        // number of open sites
        return this.openCounter;
    }

    public boolean percolates() {
        // does the system percolate?
        // return quickUnion.connected(VIRTUAL_TOP, virtualBottom);
        
        boolean ok = false;
        
        // percolates if any site in bottom row is connected with virtual top site
        for (int i = (dimension-1)*dimension +1; i <= dimension*dimension; i++) {
            if (quickUnion.connected(VIRTUAL_TOP, i)) {
                ok = true;
                break;
            }
        }
        return ok;
    }

    public static void main(String[] args) {
        Percolation percolation = new Percolation(2);

        StdOut.println(percolation.percolates());
        percolation.open(1, 1);
        percolation.open(2, 1);
        // percolation.open(3, 1);
        StdOut.println(percolation.percolates());
        // percolation.open(4, 1);
        // StdOut.println(percolation.percolates());

    }
}
