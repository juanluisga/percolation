import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private static final double CONSTANTE = 1.96;
    private final int dimension;
    private final int trials;
    private double[] threshold;

    public PercolationStats(int n, int trials) {
        // perform trials independent experiments on an n-by-n grid
        if (n < 1 || trials < 1)
            throw new java.lang.IllegalArgumentException();

        this.dimension = n;
        this.trials = trials;
        threshold = new double[trials];

    }

    public double mean() {
        // sample mean of percolation threshold

        return StdStats.mean(threshold);
    }

    public double stddev() {
        // sample standard deviation of percolation threshold
        return StdStats.stddev(threshold);
    }

    public double confidenceLo() {
        // low endpoint of 95% confidence interval
        return this.mean() - CONSTANTE * this.stddev() / Math.sqrt(this.trials);
    }

    public double confidenceHi() {
        // high endpoint of 95% confidence interval
        return this.mean() + CONSTANTE * this.stddev() / Math.sqrt(this.trials);
    }

    public static void main(String[] args) {
        // test client (described below)
        int dimension = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats stats = new PercolationStats(dimension, trials);
        Percolation percolation;
        int rowRnd;
        int colRnd;
        for (int t = 0; t < stats.trials; t++) {
            percolation = new Percolation(stats.dimension);
            do {
                rowRnd = StdRandom.uniform(1, stats.dimension + 1);
                colRnd = StdRandom.uniform(1, stats.dimension + 1);

                if (!percolation.isOpen(rowRnd, colRnd)) {
                    percolation.open(rowRnd, colRnd);
                }
            } while (!percolation.percolates());
            stats.threshold[t] = percolation.numberOfOpenSites() / Math.pow(dimension, 2);
        }
        StdOut.println("mean \t\t\t= " + stats.mean());
        StdOut.println("stddev \t\t\t= " + stats.stddev());
        StdOut.println("95% confidence interval = [" + stats.confidenceLo() + ", " + stats.confidenceHi() + "]");
    }
}